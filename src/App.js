import React, {Component} from 'react';


class App extends Component {

  state = {
    mouseDown: false,
    pixelsArray: []
  };

  componentDidMount() {
    this.websocket = new WebSocket('ws://localhost:8000/pixels');

    this.websocket.onmessage = pixelsArray => {
      const newArray = JSON.parse(pixelsArray.data);
      switch (newArray.type) {
        case "NEW_PIXELS_ARRAY":
          newArray ? this.draw(newArray.array) : null;
        break;
      }
    };
  }

  draw = pixelsArray => {
    this.ctx = this.canvas.getContext('2d');

    // отображение в canvas в пикселях
    // this.pixel = this.ctx.createImageData(1, 1);
    // this.pixel.data[0] = 0;
    // this.pixel.data[1] = 0;
    // this.pixel.data[2] = 0;
    // this.pixel.data[3] = 255;


    // отображение в canvas в виде кругов
    this.ctx.beginPath();
    this.ctx.strokeStyle = 'rgb(200, 0, 0)';
    pixelsArray.map(item => (
      this.ctx.arc(item.x, item.y, item.radius, item.begin, item.end)
    ));
    this.ctx.stroke();
  };

  canvasMouseMoveHandler = event => {

    if (this.state.mouseDown) {
      event.persist();
      this.setState(prevState => {
        return {
          pixelsArray: [...prevState.pixelsArray, {
            x: event.clientX,
            y: event.clientY,
            radius: 10,
            begin: 0,
            end: 2 * Math.PI
          }]
        };
      });

      this.context = this.canvas.getContext('2d');

      // рисование пикселями в canvas
      // this.imageData = this.context.createImageData(1, 1);
      // this.d = this.imageData.data;
      //
      // this.d[0] = 0;
      // this.d[1] = 15;
      // this.d[2] = 30;
      // this.d[3] = 255;
      // this.context.putImageData(this.imageData, event.clientX, event.clientY);


      // рисование кругами в canvas
      this.context.beginPath();
      this.context.arc(event.clientX, event.clientY, 10, 0, 2 * Math.PI);
      this.context.strokeStyle = 'rgb(200, 0, 0)';
      this.context.stroke();
    }
  };

  mouseDownHandler = () => {
    this.setState({mouseDown: true});
  };

  mouseUpHandler = event => {
    event.preventDefault();
    this.websocket.send(JSON.stringify({
      type: "CREATE_PIXELS",
      array: this.state.pixelsArray
    }));
    this.setState({mouseDown: false, pixelsArray: []});
  };

  render() {
    return (
      <div>
        <canvas
          ref={elem => this.canvas = elem}
          style={{border: '1px solid black'}}
          width={800}
          height={600}
          onMouseDown={this.mouseDownHandler}
          onMouseUp={this.mouseUpHandler}
          onMouseMove={this.canvasMouseMoveHandler}
        />
      </div>
    );
  }
}

export default App;
